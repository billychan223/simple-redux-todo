import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import TaskList from "./page/TaskList";

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <TaskList />
    </QueryClientProvider>
  );
}

export default App;
