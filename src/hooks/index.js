import { useRef, useEffect } from "react";
import { useQuery } from "react-query";
import axios from "axios";

const getTasks = async () => {
  const { data } = await axios.get(
    "https://jsonplaceholder.typicode.com/todos"
  );

  return data.splice(0, 10).map((item) => ({
    id: item.id,
    name: item.title,
    completed: item.completed,
  }));
};

export function useTasks() {
  return useQuery("tasks", getTasks);
}

export function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}
