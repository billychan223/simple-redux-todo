import { createSlice } from "@reduxjs/toolkit";

export const tasksSlice = createSlice({
  name: "tasks",
  initialState: [],
  reducers: {
    insert: (state, action) => {
      if (Array.isArray(action.payload)) {
        state = [...state, ...action.payload];
      }
      state.push(action.payload);
      return state;
    },
    remove: (state, action) => {
      state = state.filter((obj) => action.payload !== obj.id);
      return state;
    },
    update: (state, action) => {
      state = action.payload;
      return state;
    },
  },
});

// Action creators are generated for each case reducer function
export const { insert, remove, update } = tasksSlice.actions;

export default tasksSlice.reducer;
