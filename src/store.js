import { configureStore } from "@reduxjs/toolkit";
import { loadState, saveState } from "./localstorage";
import { throttle } from "lodash";
import tasksReducer from "./slice/tasksSlice";

const persistedState = loadState();

export const store = configureStore({
  reducer: {
    tasks: tasksReducer,
  },
  preloadedState: persistedState,
});

store.subscribe(
  throttle(() => {
    saveState({
      tasks: store.getState().tasks,
    });
  }, 1000)
);
